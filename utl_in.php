<?php

date_default_timezone_set('Europe/Berlin');

if (strpos($db_path, '.sqlite') !== false)
	$db_link = new SQLite3($db_path) or die('Unable to open database '.$db_path);

?>
