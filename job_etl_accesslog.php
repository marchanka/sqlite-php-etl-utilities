<?php

//------------------------------------------------------------
/* Call input arguments

php -f /path_to_etl/job_etl_accesslog.php domain.info access_log n 
0 - job_etl_accesslog.php
1 - src_name: domain_name
2 - src_file: access_log
3 - force_restart: y/n
4 - start_step: brocken step name
5 - ftp_host
6 - ftp_path
7 - ftp_user
8 - ftp_pass
*/
//------------------------------------------------------------
// Config

$src_path = getcwd();
// input parameter
// src_name
if (defined('STDIN')) $src_name = (!empty($argv[1])?($argv[1]):('my_project'));
else $src_name = (!empty($_GET['src_name'])?($_GET['src_name']):('my_project'));
$src_name = strtolower($src_name);
// src_file
if (defined('STDIN')) $src_file = (!empty($argv[2])?($argv[2]):('access_log'));
else $src_file = (!empty($_GET['src_file'])?($_GET['src_file']):('access_log'));
$src_file = strtolower($src_file);
// force_restart
if (defined('STDIN')) $force_restart = (!empty($argv[3])?($argv[3]):('n'));
else $force_restart = (!empty($_GET['force_restart'])?($_GET['force_restart']):('n'));
$force_restart = strtolower($force_restart);
// step_name
if (defined('STDIN')) $start_step = (!empty($argv[4])?($argv[4]):('none'));
else $start_step = (!empty($_GET['start_step'])?($_GET['start_step']):('none'));
$start_step = strtoupper($start_step);
// ftp
if (defined('STDIN')) $ftp_host = (!empty($argv[5])?($argv[5]):('ftp.'));
else $ftp_host = (!empty($_GET['ftp_host'])?($_GET['ftp_host']):('ftp.'));
$ftp_host = strtolower($ftp_host);
if (defined('STDIN')) $ftp_path = (!empty($argv[6])?($argv[6]):('/logs/'));
else $ftp_path = (!empty($_GET['ftp_path'])?($_GET['ftp_path']):('/logs/'));
if (defined('STDIN')) $ftp_user = (!empty($argv[7])?($argv[7]):(''));
else $ftp_user = (!empty($_GET['ftp_user'])?($_GET['ftp_user']):(''));
if (defined('STDIN')) $ftp_pass = (!empty($argv[8])?($argv[8]):(''));
else $ftp_pass = (!empty($_GET['ftp_pass'])?($_GET['ftp_pass']):(''));

$trg_file = $src_path.'/account/'.$src_name.'/stage/'.$src_file; // to
$db_path = $src_path.'/account/'.$src_name.'/data/analyselog_dwh.sqlite';

// ETL name
$etl_name = strtoupper('ETL_ACCESS_LOG');

$step_name = '';
$step_trigger = 0;
$sid = 0;
$id = 0;

include $src_path.'/utl_func.php';
include $src_path.'/utl_in.php';

//------------------------------------------------------------
// Error handler

function warning_handler($code, $text, $file, $line) {
	global $src_path, $db_path, $db_link, $sid, $etl_name, $step_name;

	if ($sid) fnc_utl_put_log ($db_link, $sid, $etl_name, 'ETL_ERROR', $step_name.' '.$text);
	include $src_path.'/utl_out.php';

	exit($text);
}

set_error_handler('warning_handler', E_ALL);

// start etl
$sid = fnc_utl_get_sid($db_link, $etl_name, $force_restart);
if ($sid) fnc_utl_put_log ($db_link, $sid, $etl_name, 'ETL_START', 'force_restart:'.$force_restart.' start_from:'.$start_step);


//------------------------------------------------------------
// 1. ETL FTP access log

$step_name = 'FTP_TRANSFER';
if ($start_step == 'NONE' || $start_step == $step_name) $step_trigger = 1;

if ($sid && $step_trigger) {

	if ($ftp_host && $ftp_user && $ftp_pass)
	if ($ftp_link = ftp_connect($ftp_host)) {
		ftp_login($ftp_link, $ftp_user, $ftp_pass);
		ftp_pasv($ftp_link, 1);
		ftp_chdir($ftp_link, $ftp_path);
		ftp_get($ftp_link, $trg_file, $src_file, FTP_BINARY);

		fnc_utl_put_log ($db_link, $sid, $etl_name, $step_name, 'ready. file:'.$trg_file);
	}
	else fnc_utl_put_log ($db_link, $sid, $etl_name, $step_name, 'no ftp credetials');
}

//------------------------------------------------------------
// 2. ETL truncate stage

$step_name = 'STG_ACCESS_LOG.TRUNCATE';
if ($start_step == 'NONE' || $start_step == $step_name) $step_trigger = 1;

if ($sid && $step_trigger) {
	$sql = 'DELETE FROM STG_ACCESS_LOG';
	$result = $db_link->exec($sql) or 0;
	fnc_utl_put_log ($db_link, $sid, $etl_name, $step_name, 'ready');
}

//------------------------------------------------------------
// 3. ETL read into stage

$step_name = 'STG_ACCESS_LOG';
if ($start_step == 'NONE' || $start_step == $step_name) $step_trigger = 1;

if ($sid && $step_trigger) {
	//67.221.59.195 - - [28/Dec/2012:01:47:47 +0100] "GET /files/default.css HTTP/1.1" 200 1512 "https://project.edu/" "Mozilla/4.0"
	//host ident auth   time                          method request_nk      protocol  status bytes ref                 browser
	$log_pattern = '/^([^ ]+) ([^ ]+) ([^ ]+) (\[[^\]]+\]) "(.*) (.*) (.*)" ([0-9\-]+) ([0-9\-]+) "(.*)" "(.*)"$/';
	$i = 0;
	$i_max = 1000;
	$sql = '';

	if ($file_link = fopen($trg_file,'r')) {
		// read each line and trim off leading/trailing whitespace
		while (!feof($file_link)) {
			$s = str_replace(array("\n\r","\n","\r","'"), '', trim(fgets($file_link, 4096)));
			$ipnr = '';
			$ipv = 'n.a.';
			$ref_host = 'n.a.';
			$ref_path = 'n.a.';
			$ref_query = 'n.a.';
			// match the line to the log_pattern
			if (preg_match($log_pattern, $s, $matches)) {
				$i_max--; // commit
				$i++; // next line
				// put each part of the match in an appropriately-named variable 
				list($whole_match, $host_nk, $ident, $auth, $time_nk, $method, $request_nk, $protocol, $status, $bytes, $ref, $browser) = $matches;
				if ($ref == '-' || empty($ref)) {
					$ref = 'n.a.';
				} else {
					$ref = parse_url($ref);
					if ( isset($ref['host']) ) $ref_host = $ref['host'];
					if ( isset($ref['path']) ) $ref_path = $ref['path'];
					if ( isset($ref['query']) ) $ref_query = $ref['query'];
				}
				// ipv converting
				if ( sizeof($host_nk) > 15 && sizeof($host_nk) <= 39 && strpos($host_nk, ':') > 0 ) {
					$ipv = 'IPV6';
					$ipnr = ip2long_v6($host_nk);
				} else {
					$ipv = 'IPV4';
					$ipnr = ip2long($host_nk);
				}
				// final query
				$sql .= '(\''.$i.'\',\''.$host_nk.'\',\''.$ipnr.'\',\''.$ipv.'\',\''.$ident.'\',\''.$auth.'\',\''.$time_nk.'\',\''.$method.
					'\',\''.$request_nk.'\',\''.$protocol.'\',\''.$status.'\',\''.$bytes.'\',\''.$ref_host.
					'\',\''.$ref_path.'\',\''.$ref_query.'\',\''.$browser.'\'), ';
			}
			if ($i_max == 0 || feof($file_link)) {
				$sql = 'INSERT INTO STG_ACCESS_LOG (line_nk, host_nk, ip_nr, ipv, ident, auth, time_nk, method, request_nk, protocol, status, bytes, referrer_host, referrer_path, referrer_query, browser) VALUES '.substr($sql, 0, -2);
				//print $sql."\n\n";
				$i_max = 1000;
				$result = $db_link->exec($sql) or 0;
				$sql = '';
			}
		}
		fclose($file_link);
		fnc_utl_put_log ($db_link, $sid, $etl_name, $step_name, 'ready. Affected rows: *** '.$i.' ***');
	}
}

//------------------------------------------------------------
// 4. ETL propagation

$step_name = 'DIM_USER_AGENT.USER_AGENT_NK';
if ($start_step == 'NONE' || $start_step == $step_name) $step_trigger = 1;

if ($sid && $step_trigger) {
	$sql = get_content_from_path($src_path.'/account/'.$src_name.'/sql/ETL_INS-ACCESS_LOG-USER_AGENT_NK.sql');
	$result = $db_link->exec($sql) or 0;
	fnc_utl_put_log ($db_link, $sid, $etl_name, $step_name, 'ready');
}

//------------------------------------------------------------
// 5. ETL propagation

$step_name = 'DIM_REQUEST.REQUEST_NK';
if ($start_step == 'NONE' || $start_step == $step_name) $step_trigger = 1;

if ($sid && $step_trigger) {
	$sql = get_content_from_path($src_path.'/account/'.$src_name.'/sql/ETL_INS-ACCESS_LOG-REQUEST_NK.sql');
	$result = $db_link->exec($sql) or 0;
	fnc_utl_put_log ($db_link, $sid, $etl_name, $step_name, 'ready');
}

//------------------------------------------------------------
// 6. ETL propagation

$step_name = 'DIM_REFERRER.NK';
if ($start_step == 'NONE' || $start_step == $step_name) $step_trigger = 1;

if ($sid && $step_trigger) {
	$sql = get_content_from_path($src_path.'/account/'.$src_name.'/sql/ETL_INS-ACCESS_LOG-REFERRER_NK.sql');
	$result = $db_link->exec($sql) or 0;
	fnc_utl_put_log ($db_link, $sid, $etl_name, $step_name, 'ready');
}

//------------------------------------------------------------
// 7. ETL aggregate

$step_name = 'FCT_ACCESS_USER_AGENT_DD';
if ($start_step == 'NONE' || $start_step == $step_name) $step_trigger = 1;

if ($sid && $step_trigger) {
	$param_epoch_from = get_last_event_epoch($db_link, $step_name);
	$sql = get_content_from_path($src_path.'/account/'.$src_name.'/sql/ETL_INS-FCT_ACCESS_USER_AGENT_DD.sql');
	$sql = str_replace('$param_epoch_from', $param_epoch_from, $sql);
	$result = $db_link->exec($sql) or 0;
	fnc_utl_put_log ($db_link, $sid, $etl_name, $step_name, 'ready. param_epoch_from:'.$param_epoch_from.' ('.date('d.m.Y',intval($param_epoch_from)).')');
}

//------------------------------------------------------------
// 8. ETL aggregate

$step_name = 'FCT_ACCESS_REQUEST_REF_HH';
if ($start_step == 'NONE' || $start_step == $step_name) $step_trigger = 1;

if ($sid && $step_trigger) {
	$param_epoch_from = get_last_event_epoch($db_link, $step_name);
	$sql = get_content_from_path($src_path.'/account/'.$src_name.'/sql/ETL_INS-FCT_ACCESS_REQUEST_REF_HH.sql');
	$sql = str_replace('$param_epoch_from', $param_epoch_from, $sql);
	$result = $db_link->exec($sql) or 0;
	fnc_utl_put_log ($db_link, $sid, $etl_name, $step_name, 'ready. param_epoch_from:'.$param_epoch_from.' ('.date('d.m.Y',intval($param_epoch_from)).')');
}

//------------------------------------------------------------
if ($sid) fnc_utl_put_log ($db_link, $sid, $etl_name, 'ETL_END', 'ready');

restore_error_handler();

include $src_path.'/utl_out.php';

?>
