<?php

function fnc_utl_get_sid ($db_link, $in_job_name = 'n.a.', $in_force_restart = 'n') {
	$sid = 0;
	// get last sid
	$sql = 
	 'SELECT SUM (
	  CASE WHEN start_job.JOB_NAME IS NOT NULL AND end_job.JOB_NAME IS NULL /* existed job finished */
		    AND NOT ( \'y\' = \''.$in_force_restart.'\' ) /* force restart PARAMETER */
	       THEN 1 ELSE 0
	  END ) AS IS_RUNNING
	  FROM
	    ( SELECT 1 AS dummy FROM UTL_JOB_STATUS WHERE sid = -1) d_job
	  LEFT OUTER JOIN
	    ( SELECT JOB_NAME, SID, 1 AS dummy
	      FROM UTL_JOB_STATUS
	      WHERE JOB_NAME = \''.$in_job_name.'\' /* job name PARAMETER */
		    AND STEP_NAME = \'ETL_START\'
	      GROUP BY JOB_NAME, SID
	    ) start_job /* starts */
	  ON d_job.dummy = start_job.dummy
	  LEFT OUTER JOIN
	    ( SELECT JOB_NAME, SID
	      FROM UTL_JOB_STATUS
	      WHERE JOB_NAME = \''.$in_job_name.'\'  /* job name PARAMETER */
		    AND STEP_NAME in (\'ETL_END\', \'ETL_ERROR\') /* stop status */
	      GROUP BY JOB_NAME, SID
	    ) end_job /* ends */
	  ON start_job.JOB_NAME = end_job.JOB_NAME
	     AND start_job.SID = end_job.SID';
	//echo $sql;
	$result = $db_link->query($sql) or 0;
	if ($result) {
		$sql_info = $result->fetchArray(SQLITE3_ASSOC);
		$result->finalize();
	}
	//print_r($sql_info);
	if ($sql_info['IS_RUNNING'] == 0) {
		// new job, new sid
		list($msec, $sec) = explode(' ', microtime());
		$sid = $sec.substr($msec, 2, 3); // '1491536422147'
	}
	return ($sid);
}


function fnc_utl_put_log ($db_link, $in_sid = -1, $in_job_name = 'n.a.', $in_step_name = 'n.a.', $in_step_descr = 'n.a.') {
	$log_d = mktime(0, 0, 0, date('m'), date('d'), date('Y'));
	$log_dt = mktime(date('H'), date('i'), date('s'), date('m'), date('d'), date('Y'));
	// put into log
	$sql = 
	'INSERT INTO UTL_JOB_STATUS (SID, LOG_DT, LOG_D, JOB_NAME, STEP_NAME, STEP_DESCR)
	 VALUES ('.$in_sid.', '.$log_dt.', '.$log_d.', \''.$in_job_name.'\', \''.$in_step_name.'\', \''.$in_step_descr.'\')';
	//echo $sql;
	$result = $db_link->exec($sql) or 0;
	return (($result)?($db_link->lastInsertRowID()):(0));
}


function fnc_utl_get_last_success_epoch ($db_link, $in_job_name = 'n.a.') {
	// get last job date
	$sql = 
	'SELECT COALESCE(MAX(LOG_DT), \'3600\') AS LAST_SUCCESS_EPOCH
	 FROM UTL_JOB_STATUS
	 WHERE 1 = 1
	   AND datetime(LOG_D, \'unixepoch\') < DATE(\'now\')
	   AND step_name = \'ETL_END\'
	   AND job_name = \''.$in_job_name.'\'';
	//echo $sql;
	$result = $db_link->query($sql) or 0;
	if ($result) {
		$row = $result->fetchArray(SQLITE3_ASSOC);
		$result->finalize();
	}
	return ($row['LAST_SUCCESS_EPOCH']);
}


function get_last_event_epoch ($db_link, $in_table_name = 'n.a.') {
	// get last job date
	$sql = 
	'SELECT COALESCE(MAX(EVENT_DT), \'3600\') AS LAST_EVENT_EPOCH FROM '.$in_table_name;
	//echo $sql;
	$result = $db_link->query($sql) or 0;
	if ($result) {
		$row = $result->fetchArray(SQLITE3_ASSOC);
		$result->finalize();
	}
	return ($row['LAST_EVENT_EPOCH']);
}


function get_content_from_path($path) {
	$content = 'file not found';
	if (file_exists($path)) {
		$content = '';
		$fd = fopen($path, 'r');
		while (!feof($fd))
			$content .= fgets($fd, 4096);
		fclose($fd);
	}
	return $content;
}


function put_content_to_path($content, $path) {
	$success = 1;
	if ($fd = fopen($path, 'w')) {
		if (fwrite($fd, $content) === FALSE)
			$success = 0;
		fclose($fd);
	} else
		$success = 0;
    return $success;
}


?>
