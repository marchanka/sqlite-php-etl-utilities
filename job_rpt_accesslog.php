<?php

//------------------------------------------------------------
/* Call input arguments

php -f /path_to_etl/job_rpt_accesslog.php domain.info n 
0 - job_rpt_accesslog.php
1 - src_name: domain.info
2 - force_restart: y/n
*/
//------------------------------------------------------------
// Config

$src_path = getcwd();
// input parameter
// src_name
if (defined('STDIN')) $src_name = (!empty($argv[1])?($argv[1]):('my_project'));
else $src_name = (!empty($_GET['src_name'])?($_GET['src_name']):('my_project'));
$src_name = strtolower($src_name);
// force restart
if (defined('STDIN')) $force_restart = (!empty($argv[2])?($argv[2]):('n'));
else $force_restart = (!empty($_GET['force_restart'])?($_GET['force_restart']):('n'));
$force_restart = strtolower($force_restart);

$trg_file = $src_path.'/report/'.$src_name.'.html'; // to
$db_path = $src_path.'/account/'.$src_name.'/data/analyselog_dwh.sqlite';

// ETL name
$etl_name = strtoupper('RPT_ACCESS_LOG');

$step_name = '';
$sid = 0;
$id = 0;

include $src_path.'/utl_func.php';
include $src_path.'/utl_in.php';

//------------------------------------------------------------
// Error handler

function warning_handler($code, $text, $file, $line) {
	global $src_path, $db_path, $db_link, $sid, $etl_name, $step_name;

	if ($sid) fnc_utl_put_log ($db_link, $sid, $etl_name, 'ETL_ERROR', $step_name.' '.$text);
	include $src_path.'/utl_out.php';

	exit($text);
}

set_error_handler('warning_handler', E_ALL);

// start etl
$sid = fnc_utl_get_sid($db_link, $etl_name, $force_restart);
if ($sid) fnc_utl_put_log ($db_link, $sid, $etl_name, 'ETL_START', 'force_restart:'.$force_restart);


//------------------------------------------------------------
// 1. generate report

if ($sid) {

	$sqls = array(
	'SELECT * FROM RPT_ACCESS_USER_VS_BOT',
	'SELECT * FROM RPT_ACCESS_ANNOYING_BOT',
	'SELECT * FROM RPT_ACCESS_TOP_HOUR_HIT',
	'SELECT * FROM RPT_ACCESS_USER_ACTIVE',
	'SELECT * FROM RPT_ACCESS_REQUEST_STATUS',
	'SELECT * FROM RPT_ACCESS_TOP_REQUEST_PAGE',
	'SELECT * FROM RPT_ACCESS_TOP_REQUEST_REFERRER',
	'SELECT * FROM RPT_ACCESS_NEW_REQUEST',
	'SELECT * FROM RPT_ACCESS_TOP_REQUEST_SUCCESS',
	'SELECT * FROM RPT_ACCESS_TOP_REQUEST_ERROR'
	);

	$rpt = '<!DOCTYPE html>';
	$rpt .= '<html lang="en"><title>Server Access Log Report</title>';
	$rpt .= '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
	$rpt .= '<meta name="viewport" content="width=device-width, initial-scale=1, viewport-fit=cover" />';
	$rpt .= '<link rel="stylesheet" href="style/layout.css" />';
	$rpt .= '<script src="style/js-class.js"></script>';
	$rpt .= '<script src="style/bluff.js"></script>';
	$rpt .= '<script src="style/excanvas.js"></script>';
	$rpt .= '<script>function toggle(elementId, state) { var elem = document.getElementById(elementId); if (elem) {	var visibility = elem.style.display; ';
	$rpt .= 'if(state==1 && visibility=="none") { visibility="table" }; if(state==0 && visibility=="table") { visibility="none"; }';
	$rpt .= 'if(state==-1) { visibility=(visibility=="none")?("table"):("none"); } elem.style.display=visibility;} } </script>';
	$rpt .= '<body><div id="cCont"><h2>Access Log Report '.$src_name.'</h2><div class="spacer"></div>'."\n";

	for ($j = 0; $j < sizeof($sqls); $j++) {
	$result = $db_link->query($sqls[$j]) or 0;
	$step_name = $sqls[$j];
	if (!$result) {
		fnc_utl_put_log ($db_link, $sid, $etl_name, $step_name, 'no data from '.$sqls[$j]);
	} else {
		$rtype = 'Line';
		$rtitle = '';
		$rcont = '';
		$rstyl = '';
		$rcapt = '';
		$rlink = '';
		$rhead = '';
		$rrow = '';
		$z = 0;
		while ($row = $result->fetchArray(SQLITE3_ASSOC)) {
			$rcol = 0;
			foreach ($row as $k => $v) {
				$pos = strpos($k,':');
				if ($z == 0 && $pos > 0) {
					$rtype = substr($k, 0, $pos);
					$rtitle = substr($k, $pos+2);
					if ($rtype == 'Table') {
						$rstyl = ' style="display:table;"';
						$rlink = '';
						$rcapt = '';
					} else {
						$rstyl = ' style="display:none;"';
						$rlink = '<p><a href="javascript:toggle(\'data'.$j.'\',-1)">Show/Hide the data</a></p>';
						$rcapt = '<caption>'.$rtitle.'</caption>';
					}
					$rcont .= $rlink.'<table id="data'.$j.'"'.$rstyl.'>'.$rcapt.'<thead><tr>';
				} else {
					$rhead .= '<th scope="col">'.$k.'</th>';
				}
				if ($rcol == 1) $rrow .= '<tr><th scope="row">'.$v.'</th>';
				if ($rcol > 1) $rrow .= '<td>'.$v.'</td>';
				$rcol++;
			}
			if ($z == 0) {
				$rcont .= $rhead.'</tr></thead><tbody>';
			}
			$rrow .= '</tr>';
			$z++;
		}
		$rcont .= $rrow.'</tbody></table>';
		$rpt .= '<h5>'.$rtitle.'</h5>';
		if ($rtype == 'Line' || $rtype == 'Area' || $rtype == 'BarConversion' || $rtype == 'Bar' || $rtype == 'SideBar' || 
		    $rtype == 'Line' || $rtype == 'Dot' || $rtype == 'Net' || $rtype == 'Spider' || $rtype == 'StackedArea' ||
		    $rtype == 'StackedBar' || $rtype == 'AccumulatorBar' || $rtype == 'SideStackedBar' || $rtype == 'Pie' ) {
			$rpt .= '<canvas id="graph'.$j.'" width="700" height="300"></canvas>';
			$rpt .= $rcont;
			$rpt .= '<script> /* <![CDATA[ */var g=new Bluff.'.$rtype.'("graph'.$j.'","700x300"); ';
			$rpt .= 'g.set_theme({colors:["#4167b1","#149647","#db3c26","#f8981d","#93278f","#8c564b"],';
			$rpt .= 'marker_color:"#ccc",font_color:"#000",background_colors:["#ffffff","#fffff0"]}); ';
			$rpt .= 'g.marker_count="6";g.hide_line_markers=false;g.hide_line_numbers=false; ';
			$rpt .= 'g.line_width=2;g.sort=false;g.dot_radius=5;g.hide_dots=false;g.tooltips=true; ';
			$rpt .= '/*g.x_axis_label="";*/g.legend_position="right";g.hide_legend=false; ';
			$rpt .= 'g.hide_mini_legend=false;g.hide_lines=false;g.hide_title=true; ';
			$rpt .= 'g.title_font_size=14;g.legend_font_size=12;g.marker_font_size=12;g.top_margin=1; ';
			$rpt .= 'g.right_margin=1;g.bottom_margin=1;g.left_margin=10;g.zero_degree=10; ';
			$rpt .= 'g.bar_spacing=0.8;g.minimum_value=0;g.no_data_message="No data";g.maximum_value=5; ';
			$rpt .= 'g.data_from_table("data'.$j.'"'.($rtype=='Pie'?', {orientation: "rows"}':'').'); g.draw();/* ]]> */ </script>';
		} else
			$rpt .= $rcont;
		$rpt .= '<div class="spacer"></div>'."\n";

		fnc_utl_put_log ($db_link, $sid, $etl_name, $step_name, 'ready');
	}
	}
	$rpt .= '<p>Generated: <strong>'.date('d.m.y H:i:s',time()).'</strong></p>';
	$rpt .= '</div><br /></body></html>';
	
	put_content_to_path($rpt, $trg_file);
}

//------------------------------------------------------------
// 2. truncate stage
/*
if ($sid) {
	$step_name = 'STG_ACCESS_LOG.TRUNCATE';

	$sql = 'DELETE FROM STG_ACCESS_LOG';
	$result = $db_link->exec($sql) or 0;
	fnc_utl_put_log ($db_link, $sid, $etl_name, $step_name, 'ready');
}
*/
//------------------------------------------------------------
// 3. vacuum db

if ($sid) {
	$step_name = 'VACUUM';

	$sql = 'VACUUM';
	$result = $db_link->exec($sql) or 0;
	fnc_utl_put_log ($db_link, $sid, $etl_name, $step_name, 'ready');
}

//------------------------------------------------------------
if ($sid) fnc_utl_put_log ($db_link, $sid, $etl_name, 'ETL_END', 'ready');

restore_error_handler();

include $src_path.'/utl_out.php';

?>
