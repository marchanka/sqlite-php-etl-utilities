/* Propagate the message from error log */
INSERT INTO DIM_MESSAGE (MESSAGE_NK, MODULE_NK, UPDATE_DT)
SELECT
	CLS.MESSAGE_NK,
	CLS.MODULE_NK,
	STRFTIME('%s','now') AS UPDATE_DT
FROM (
	SELECT
	MESSAGE   AS MESSAGE_NK,
	MODULE_NK AS MODULE_NK,
	STRFTIME('%s','now') AS UPDATE_DT
	FROM STG_ERROR_LOG WHERE LENGTH(MESSAGE)>1
	GROUP BY MESSAGE, MODULE_NK
) CLS
LEFT OUTER JOIN DIM_MESSAGE TRG
ON CLS.MESSAGE_NK = TRG.MESSAGE_NK AND CLS.MODULE_NK = TRG.MODULE_NK
WHERE TRG.DIM_MESSAGE_ID IS NULL

