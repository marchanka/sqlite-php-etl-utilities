/* Load fact from error log */
INSERT INTO FCT_ERROR_DD (EVENT_DT, DIM_MESSAGE_ID, LINE_CNT, IP_CNT)
WITH STG AS (
SELECT
	MESSAGE AS MESSAGE_NK,
	MODULE_NK,
	STRFTIME( '%s', SUBSTR(TIME_NK,29,4) || '-' ||
	CASE SUBSTR(TIME_NK,6,3)
	WHEN 'Jan' THEN '01' WHEN 'Feb' THEN '02' WHEN 'Mar' THEN '03' WHEN 'Apr' THEN '04' WHEN 'May' THEN '05' WHEN 'Jun' THEN '06'
	WHEN 'Jul' THEN '07' WHEN 'Aug' THEN '08' WHEN 'Sep' THEN '09' WHEN 'Oct' THEN '10' WHEN 'Nov' THEN '11'
	ELSE '12' END || '-' || SUBSTR(TIME_NK,10,2) || ' 00:00:00' ) AS EVENT_DT,
	LINE_NK,
	HOST_NK
FROM STG_ERROR_LOG
)
SELECT
	CAST(STG.EVENT_DT AS INTEGER) AS EVENT_DT,
	DIM.DIM_MESSAGE_ID,
	COUNT(LINE_NK) AS LINE_CNT,
	COUNT(DISTINCT HOST_NK) AS IP_CNT
FROM STG,
     DIM_MESSAGE DIM
WHERE STG.MESSAGE_NK = DIM.MESSAGE_NK
  AND STG.MODULE_NK = DIM.MODULE_NK
  AND CAST(STG.EVENT_DT AS INTEGER) > $param_epoch_from /* load epoch date */
  AND CAST(STG.EVENT_DT AS INTEGER) < strftime('%s', date('now', 'start of day'))
GROUP BY STG.EVENT_DT, DIM.DIM_MESSAGE_ID

