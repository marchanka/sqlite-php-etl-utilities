
-- Author: Andrey Marchenko. andrey@marchenko.de 

-- detail usage
DROP VIEW IF EXISTS RPT_ACCESS_TOP_REQUEST_SUCCESS;

CREATE VIEW IF NOT EXISTS RPT_ACCESS_TOP_REQUEST_SUCCESS
AS
SELECT
  1 AS 'Table: Top Successful Requests',
  REQ.REQUEST_NK AS 'Request',
  'Successful' AS 'Request Status',
  ROUND(SUM(FCT.LINE_CNT) / 14.0, 1) AS 'Hits per Day',
  ROUND(SUM(FCT.IP_CNT) / 14.0, 1) AS 'IPs per Day',
  ROUND(SUM(FCT.BYTES)/1000 / 14.0, 1) AS 'KB per Day'
FROM
  FCT_ACCESS_REQUEST_REF_HH FCT,
  DIM_REQUEST_V_ACT REQ
WHERE FCT.DIM_REQUEST_ID = REQ.DIM_REQUEST_ID
  AND FCT.STATUS_GROUP IN ('Successful', 'Redirection')
  AND datetime(FCT.EVENT_DT, 'unixepoch') >= date('now', '-14 day')
GROUP BY REQ.REQUEST_NK
ORDER BY 4 DESC
LIMIT 20

