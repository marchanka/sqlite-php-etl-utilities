
-- Author: Andrey Marchenko. andrey@marchenko.de 

-- etl status log
DROP VIEW IF EXISTS RPT_UTL_JOB_STATUS;

CREATE VIEW IF NOT EXISTS RPT_UTL_JOB_STATUS
AS
SELECT
  1 AS 'Table: ETL Job Status',
  strftime('%d.%m.%Y', datetime(START_DT, 'unixepoch')) AS 'Day',
  strftime('%H:%M', datetime(START_DT, 'unixepoch')) AS 'Start at',
  SOURCE AS 'Source',
  LAYER AS 'Layer',
  JOB_NAME AS 'ETL Job Name',
  STEP_CNT AS 'Step Number',
  AFFECTED_ROWS AS 'Affected Rows',
  JOB_DURATION_SEC AS 'Duration sec.',
  STATUS AS 'Status',
  STEP_LOG AS 'Message'
FROM UTL_JOB_STATUS_V
WHERE datetime(START_DT, 'unixepoch') >= date('now', '-14 day')
ORDER BY SID DESC


