
-- Author: Andrey Marchenko. andrey@marchenko.de 

-- daily Statistic
DROP VIEW IF EXISTS RPT_ACCESS_NEW_REQUEST;

CREATE VIEW IF NOT EXISTS RPT_ACCESS_NEW_REQUEST
AS
SELECT
1 AS 'Table: New Requests',
REQ.REQUEST_NK AS 'Request',
SUM(FCT.LINE_CNT) AS 'Hits',
ROUND(1.0*SUM(FCT.BYTES)/1000, 1) AS 'Traffic KB',
strftime('%d.%m.%Y %H:%M', datetime(MIN(FCT.EVENT_DT), 'unixepoch')) AS 'First Visit',
strftime('%d.%m.%Y %H:%M', datetime(MAX(FCT.EVENT_DT), 'unixepoch')) AS 'Last Visit'
FROM (
	SELECT
	REQUEST_NK, DIM_REQUEST_ID
	FROM DIM_REQUEST_V_ACT
	ORDER BY DIM_REQUEST_ID DESC
	LIMIT 20
) REQ,
FCT_ACCESS_REQUEST_REF_HH FCT
WHERE FCT.DIM_REQUEST_ID = REQ.DIM_REQUEST_ID
  AND datetime(FCT.EVENT_DT, 'unixepoch') >= date('now', '-30 day')
GROUP BY REQ.REQUEST_NK
ORDER BY SUM(FCT.LINE_CNT) DESC

