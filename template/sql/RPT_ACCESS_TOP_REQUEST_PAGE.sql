
-- Author: Andrey Marchenko. andrey@marchenko.de 

-- houtrly usage
DROP VIEW IF EXISTS RPT_ACCESS_TOP_REQUEST_PAGE;

CREATE VIEW IF NOT EXISTS RPT_ACCESS_TOP_REQUEST_PAGE
AS
SELECT
  1 as 'SideBar: Top User Success Requests',
  SUBSTR(REQ.REQUEST_NK,1,30) AS 'Request',
  ROUND(SUM(FCT.IP_CNT) / 14.0, 1) AS 'IPs per Day',
  ROUND(SUM(FCT.LINE_CNT) / 14.0, 1) AS 'Hits per Day'
FROM
  FCT_ACCESS_REQUEST_REF_HH FCT,
  DIM_REQUEST_V_ACT REQ
WHERE FCT.DIM_REQUEST_ID = REQ.DIM_REQUEST_ID
  AND INSTR(REQ.REQUEST_NK,'.') = 0 /* page only */
  AND REQ.REQUEST_NK != '/' AND INSTR(REQ.REQUEST_NK,'cms') = 0 /* info page */
  AND FCT.AGENT_BOT != 'n.a' /* users only */
  AND FCT.STATUS_GROUP != 'Client Error'
  AND FCT.STATUS_GROUP != 'Server Error'
  AND datetime(FCT.EVENT_DT, 'unixepoch') >= date('now', '-14 day')
GROUP BY SUBSTR(REQ.REQUEST_NK,1,30)
ORDER BY 3 DESC
LIMIT 8
