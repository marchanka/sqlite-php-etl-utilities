
DROP TABLE IF EXISTS FCT_ACCESS_USER_AGENT_DD;
CREATE TABLE FCT_ACCESS_USER_AGENT_DD (
  EVENT_DT            INTEGER NOT NULL DEFAULT 0,
  DIM_USER_AGENT_ID   INTEGER NOT NULL DEFAULT -1,
  DIM_HTTP_STATUS_ID  INTEGER NOT NULL DEFAULT -1,
  PAGE_CNT            INTEGER NOT NULL DEFAULT 0,
  FILE_CNT            INTEGER NOT NULL DEFAULT 0,
  REQUEST_CNT         INTEGER NOT NULL DEFAULT 0,
  LINE_CNT            INTEGER NOT NULL DEFAULT 0,
  IP_CNT              INTEGER NOT NULL DEFAULT 0,
  BYTES               INTEGER NOT NULL DEFAULT 0,
  UNIQUE (EVENT_DT, DIM_USER_AGENT_ID, DIM_HTTP_STATUS_ID)
)

