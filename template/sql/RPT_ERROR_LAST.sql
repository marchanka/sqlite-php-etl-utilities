
-- Author: Andrey Marchenko. andrey@marchenko.de 

-- daily Statistic
CREATE VIEW IF NOT EXISTS RPT_ERROR_LAST
AS
SELECT
1 as 'Table: Error Log',
strftime('%d.%m.%Y', datetime(FCT.EVENT_DT, 'unixepoch')) AS 'Day',
strftime('%H:%M', datetime(MSG.UPDATE_DT, 'unixepoch')) AS 'Time',
MSG.MODULE_NK AS 'Module',
MSG.MESSAGE_NK AS 'Error',
FCT.LINE_CNT AS 'Requests',
FCT.IP_CNT AS 'IPs'
FROM
  FCT_ERROR_DD FCT,
  DIM_MESSAGE MSG
WHERE FCT.DIM_MESSAGE_ID = MSG.DIM_MESSAGE_ID
ORDER BY FCT.EVENT_DT DESC
LIMIT 30
