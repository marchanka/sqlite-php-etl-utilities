/* Propagate the request from access log */
INSERT INTO DIM_REQUEST (REQUEST_NK, UPDATE_DT)
SELECT
	CLS.REQUEST_NK,
	STRFTIME('%s','now') AS UPDATE_DT
FROM (
	SELECT DISTINCT
	CASE WHEN INSTR(REQUEST_NK,'?')>0 THEN SUBSTR(REQUEST_NK, 1, INSTR(REQUEST_NK,'?')-1)
	ELSE REQUEST_NK END AS REQUEST_NK
	FROM STG_ACCESS_LOG WHERE LENGTH(REQUEST_NK)>0
) CLS
LEFT OUTER JOIN DIM_REQUEST_V_ACT TRG
ON CLS.REQUEST_NK = TRG.REQUEST_NK
WHERE TRG.DIM_REQUEST_ID IS NULL

