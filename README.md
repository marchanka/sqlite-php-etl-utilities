# SQLite PHP ETL Utilities

Extract transform load data into SQLite data base

	
Description

This repository contains an utilities to control and log the extract transform and load procedures written in PHP and SQLite.
ETL is an queue of SQL statements which execute in a steps.
Every ETL Job gets an unique execution session identification like SID.
The predefined status like ETL_START/ETL_END/ETL_ERROR is used to prevent parallel execution of the same ETL.
Input parameters of different data types could be used for custom control of ETL statements. Like initial/delta load, load with date filter.
Some examples of ETL will be posted on www.marchenko.de

utl_func.php - function to save and read ETL logs into SQLite database

utl_in.php and utl_out.php - connector to the SQLite database

Bluff tools is using for reporting. http://bluff.jcoglan.com/api.html

Instruction

1. Create account folder from template
cp -ar /path_to_etl/template/ /path_to_etl/account/domain.project/

2. Put your access.log file into /path_to_etl/account/domain.project/stage/
Or take it automaticaly from ftp within next step

3. Call ETL

go to folder: cd /path_to_etl

and run script: php -f /path_to_etl/job_etl_accesslog.php domain_name access_log n

Parameters:

0 - job_etl_accesog.php

1 - src_name: domain_name

2 - src_file: access_log

3 - force_restart: y/n

4 - start_step: brocken step name

5 - ftp_host

6 - ftp_path

7 - ftp_user

8 - ftp_pass

For example php -f /path_to_etl/job_etl_accesslog.php domain.project access_log y DIM_USER_AGENT.USER_AGENT_NK

will continue the ETL queue from step with propagation into user agent

4. Call report

go to folder: cd /path_to_etl and run script: php -f /path_to_etl/job_rpt_accesslog.php domain.project n

0 - job_rpt_accesslog.php

1 - src_name: domain.info

2 - force_restart: y/n



